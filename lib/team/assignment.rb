require 'forwardable'

module Gitlab
  module Homepage
    class Team
      class Assignment
        extend Forwardable

        attr_reader :member, :project

        delegate username: :member

        def initialize(member, project, role)
          @member = member
          @project = project
          @role = role.to_s
        end

        def responsibility
          # e.g. trainee_maintainer => trainee maintainer
          @role.split(' ')[0].tr('_', ' ')
        end

        def description?
          !description.to_s.empty?
        end

        def description
          @role.split(' ')[1]
        end

        def owner?
          responsibility == 'owner'
        end

        def maintainer?
          responsibility == 'maintainer'
        end

        def trainee_maintainer?
          responsibility == 'trainee_maintainer'
        end

        def reviewer?
          responsibility == 'reviewer'
        end
      end
    end
  end
end
