---
layout: markdown_page
title: "GitLab Alliances Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Open to Collaboration

GitLab is an open product with a Core, open-source edition, and a enterprise, open-core edition, with additional features that are closed-sourced.

We're open to integrations with companies and tools that are interested in harnessing GitLab's platform capabilities, user flows and data with their products.

If you want to talk to us about a partnership please create a shared Google Doc that includes: details about the technical integration, end user benefits and business relationship that you would ideally want. Then please contact [Brandon Jung](mailto:bjung@gitlab.com), our VP Alliances.


## Partner definitions

### Services partners

Primary monetization is through the sale of services.  This can be a one-time implementation, ongoing support, or outsourcing.
1. Global Systems Integrators - have a large global workforce and can deliver on almost any customer need. Examples: Accenture, Deloitte, TCS, Wipro
1. Regional Systems Integrators - large workforce but with single continental focus and a more limited offering of services. Examples: CI&T, Slalom
1. Boutique Systems Integrators - very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it. Examples - Managed Service Providers - provide ongoing support for solutions/applications. Examples: Rackspace

### [Resellers](https://about.gitlab.com/resellers/) 

Primary monetization is through reselling GitLab.
1. VAR/VAD (Value Added Reseller or Distributor) - Channel services including resale, implementation, contracting, support, financing etc.
1. DMR (Direct Market Reseller)-  primary business is resale of the software, often does not implement.  Value are the contracts that these partners have in place with customers.
1. Training Partners - focus on training companies, teams and often certifications

### Technology partners

Primary monetization is through the sales of software licensing/support that integrates with GitLab
1. Upstream Technology Partners. Examples: JetBrains, Visual Studio, Eclipse, Slack, Cloudability
1. Integrated/Competitive Technology Partners. Examples: Jenkins, Codeship, Shippable, TeamCity, Atlassian. Need very clear agreement from sales, product, and engineering on how we prioritize working with these partners.  
1. Downstream/Platform Technology Partners. Examples: AWS, Google, Azure, IBM, Salesforce
1. Sell-to Technology Partners - Most of CNCF and majority of tech partners.

## Criteria for Successful Partnerships

1. Exposure - Partnerships which generate more exposure to new segments of audiences, integrations are a good example.
1. Product usage familiarity - more people using GitLab but not necessarily installing their own instance (GitLab.com), open source projects as an example.
1. Adoption - partnerships attracting more people to adopt GitLab for their own instance.  Upstream and downstream partners help here.
1. Revenue - Revenue generating partnerships.  Users come first but focus on revenue has ensured we have the revenue for sustainable growth.
1. Strategic - partners we find will add strategic value to our long term positioning and often around competitive situations.
 
## GitLab Delivery Models
There are many ways that GitLab can be both installed (https://about.gitlab.com/install) and once installed many deployment environments that GitLab can target.  Below is a structure to think about those options and some of the trade-offs that are made depending on the model.
* Self-managed - customer downloads, installs and maintains themselves
* GitLab Hosted - same code as self managed but maintained by a third party
* Marketplace - self-managed but purchased through marketplaces
* GLaaSTS - fully managed (by the cloud provider) private instance of GitLab.  Similar to the managed database options clouds offer.
* GitLab.com - fully managed multi tenent offer by GitLab Inc.

| Delivery Model | Self-Managed | GitLab Hosted | Marketplace | GLaaSTS | GitLab.com |
| -------------- | ------------ | ------------- | ----------- | ------- | ---------- |
| State | Active | Depricated | Soon | Future | Active |
| Tenents | Single | Single | Single | Single | Multi |
| Installation Package | Omnibus/Helm | Omnibus/Helm | Omnibus/Helm | Helm | Moving to Helm |
| Managed | Self | Partner | Self | Cloud | GitLab |
| Billing Ownership | GitLab/Partner | Partner | GCP, AWS, DO, etc | Cloud | GitLab |
| Infrastructure Incl. | no | yes | yes | yes | yes |
| Partners | SI's/VAR's | MSP's | Clouds | Clouds | GitLab/Partner |
| Sales Focus | Hybrid/Multi-Cloud | Partner | Self-service | Self-service | Self-service |
| Pricing | user/yr | user/yr | user/month or hour | user/yr | user/yr |

## Acquisitions

If you are interested to inquire about a potential acquisition of your company please visit our [acquisition offer](/handbook/alliances/acquisition-offer) page.

## Workflow guidelines

### Handling inbound alliance requests if at GitLab

If you've received an inbound alliance request please post a new message to the #alliances Slack channel with the brief description of the request. Once posted, the alliances team will declare who will take lead on that request and the necessary next steps to take.

### Google docs
1. Gdocs which are shared with the partners should be set to "Anyone with the link". Internal Gdocs should be set to "GitLab".
1. The following structure should be used for Gdoc names: "PARTNER NAME and GitLab".

### Cloud images

See the [Cloud image process](./cloud-images/) page for guidelines.
