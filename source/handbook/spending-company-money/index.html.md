---
layout: markdown_page
title: "Spending Company Money"
---

## On this page
{:.no_toc}

- TOC
{:toc}

In keeping with our values of results, freedom, efficiency, frugality, and boring solutions, we expect GitLabbers to take responsibility to determine what they need to purchase or expense in order to do their jobs effectively. We don't want you to have to wait with getting the items that you need to get your job done. You most likely know better than anyone else what the items are that you need to be successful in your job. The guidelines below describe what people in our team commonly expense.

1. Spend company money like it is your **own** money. _No, really_.
1. You don't have to [ask permission](https://m.signalvnoise.com/if-you-ask-for-my-permission-you-wont-have-my-permission-9d8bb4f9c940) before making purchases **in the interest of the company**. When in doubt, do **inform** your manager before the purchase, or as soon as possible after the purchase.
1. It is uncommon for you to need all of the items listed below. Use your best judgement and buy them as you need them. If you wonder if something is common, feel free to ask People Ops (and in turn, People Ops should update the list).
1. It is generally easiest and fastest for you to make the purchases yourself, but feel free to reach out to People Ops if you would like help in [acquiring some items](/handbook/general-onboarding/onboarding-processes/#ordering-supplies). Just include a link and your shipping address in an email, and People Ops will be happy to place the order.
1. You may privately use GitLab property, a MacBook for example, to check your private e-mails or watch a movie as long as it does not violate the law, harm GitLab, or interfere with [Intellectual Property](/handbook/general-guidelines/#sts=Intellectual Property).
1. If you make a purchase that will cost GitLab $1000 USD (or over), this is classed as company property, you will be required to return the item(s) if you leave the company.
1. Employees: file your expense report no later than 7 days after the end of the calendar quarter that you made the purchase in. Contractors: include receipts with your invoices.
1. Any non-company expenses paid with a company credit card will have to be reported to your manager as soon as possible and **refunded** in full within 14 days.
1. **Items.** The company will pay for the following items if you **need it for work or use it mainly for business**, and local law allows us to pay for it without incurring payroll taxes. Items paid for by the company are property of the company and need to be reported with serial numbers etc. to People Ops for proper [asset tracking](/handbook/finance/asset-tracking/). Since these items are company property, you do not need to buy insurance for them unless it is company policy to do so (for example, at the moment we do not purchase Apple Care), but you do need to report any loss or damage to PeopleOps as soon as it occurs. Links in the list below are to sample items, other options can be considered. It's also recommended to consult the `#questions` channel in slack on the latest recommendations other GitLabbers have been purchasing:
    *  Notebook:
        * To request a new MacBook please email peopleops@gitlab.com.  Please note that only People Ops should order computers, expense reports with laptops may be denied.
          * Engineers - We recommend getting a [MacBook Pro 15-inch retina with 512GB of storage and 16GB of memory](https://www.apple.com/shop/buy-mac/macbook-pro/15-inch) or similar.
          * Product Managers and Solutions Architects - We recommend getting a [MacBook Pro 13-inch retina with 256GB of storage and 16GB of memory](http://www.apple.com/shop/buy-mac/macbook-pro/13-inch)
          * Others - We recommend getting a [MacBook Pro 13-inch retina with 256GB of storage and 8GB of memory](http://www.apple.com/shop/buy-mac/macbook-pro/13-inch)
        * We do not allow the purchase of any Windows machines. Windows makes it harder to work with git and ruby from the command line, and Windows has too many security risks associated with it.
        * We strongly encourage Macs, but for Developers and Production Engineers _only_ we do allow Linux. Note that [1Password](/handbook/security/#1password-guide) does not yet have a native client for Linux, but [there is a browser extension](https://support.1password.com/getting-started-1password-x/).
        * Many team members can use their company issued laptop until it breaks. If your productivity is suffering, you can request a new laptop. The typical expected timeframe for this is about three years, but it can depend on your usage and specific laptop.
    *  USB-C Adapter for New Macbooks: Max price **80 USD**
    *  Notebook carrying bag: Max price **60 USD**
    *  External monitor: Max price **380 USD**
    *  HDMI cable (make sure you get the proper cable for your laptop): Max price **15 USD**
    *  Portable 15" external monitor: Max price **200 USD**
    *  Webcam: Max price **50 USD**
    *  Ethernet connector: Max price **20 USD**
    *  [Earpods](https://www.apple.com/shop/product/MNHF2AM/A/earpods-with-35-mm-headphone-plug) are strongly recommended instead of using notebook speakers and microphones since these can cause an echo. Max price: **30 USD**
    *  Headphones: Max price **60 USD**
    *  Keyboard and mouse set: Max price **120 USD**
    *  Height-adjustable desk: Max price **500 USD**
    *  Ergonomic chair: Max price **200 USD**
    *  Notebook stand: Max price **90 USD**
    *  Work-related books
    *  [Yubikey](https://www.yubico.com/store/)
    *  [USB hub](https://www.amazon.com/Kensington-UH4000-Port-USB-3-0/dp/B00O9RPP28/)
    *  We have central [license management](/handbook/tools-and-tips/#jetbrains) for you to request licenses for JetBrains' products like RubyMine / GoLand
    *  [Little Snitch](https://www.obdev.at/products/littlesnitch/index.html) is an excellent personal firewall solution for MacOS. Recommended to monitor application network communications.
    *  Not sure what to buy? Look at our [equipment examples page](/handbook/spending-company-money/equipment-examples) to see some of the items that other GitLabbers have purchased, and please consider adding to the list if there's something you'd like to share.
1. **Expenses.** The company will reimburse for the following expenses if you need it for work or use it mainly for business, and local law allows us to pay for it without incurring taxes:
    *  Mileage is reimbursed according to local law: [US rate per mile](http://www.irs.gov/Tax-Professionals/Standard-Mileage-Rates), or [rate per km](http://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/zakelijk/auto_en_vervoer/auto_van_de_onderneming/autokosten/u_rijdt_in_uw_eigen_auto) in the Netherlands. Add a screenshot of a map to the expense in Expensify indicating the mileage.
    *  Internet connection subscription.
        *  For employees in the Netherlands: fill in and sign the [Regeling Internet Thuis](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) form and it to People Ops.
    *  VPN service subscription. Using a VPN is recommended when you are regularly working on public WiFis or hot spots.
    *  Mobile subscription, we commonly pay for that if you call a lot as a salesperson or executive, or if your position requires participation in an oncall rotation.
    *  Telephone land line (uncommon, except for positions that require a lot of phone calls)
    *  Skype calling credit, we can autofill your account (uncommon, since we mostly use Google Hangouts, Appear.in, Zoom, and WebEx)
    *  Google Hangouts calling credit
    *  Office space
        * If working from home is not practical for a day, a month, a year, forever, please feel free to rent out a co-working space at the expense of the company.
    *  Work-related online courses
    *  The company will pay for all courses related to learning how to code (for example [Learning Rails on Codecademy](https://www.codecademy.com/learn/learn-rails)), and you may also allocate work time to take courses that interest you. If you are new to development, we encourage you to learn Git through GitLab, and feel free to ask any questions in the #git-help Slack channel.
    *  Work-related conferences, including travel, lodging, and meals. If total costs exceed [$500](/handbook/people-operations/global-compensation/#exchange-rates), reimbursement requires prior approval from your manager.
        * We encourage people to be speakers in conferences. Tweets about the talk are welcome!
        * We suggest to the attendees bring and share a post or document about the news and interesting items that can bring value to our environmnent.
    *  Holiday Party Budget: [50 USD](/handbook/people-operations/global-compensation/#exchange-rates) per person. We encourage GitLabbers to self organize holiday parties with those close by.
    *  For travel to other team members please see our [visiting grant](/handbook/incentives/#visiting-grant).
    *  Business travel upgrades per round-trip (i.e. not per each leg of the flight):
        * Up to the first [EUR 300](/handbook/people-operations/global-compensation/#exchange-rates) for an upgrade to Business Class on flights longer than 8 hours.
        * Upgrade to Economy Plus if you’re taller than 1.95m / 6’5”.
        * Up to the first [EUR 100](/handbook/people-operations/global-compensation/#exchange-rates) for an upgrade to Economy Plus (no height restriction) on flights longer than 8 hours.
    *  GitLab does not cover expenses for Significant others or family members for travel or immigration. This includes travel and visas for the GitLab summit.
    *  Something else? No problem, and consider adding it to this list if others can benefit as well.
1. **Expense Reimbursement**
    *  If you are a contractor, please submit an invoice (a template can be found [here](/handbook/finance/#invoice-template-and-where-to-send)) with receipts attached to <payroll@gitlab.com>.
    *  If you are an employee, GitLab uses Expensify to facilitate the reimbursement of your expenses. As part of onboarding you will receive an invitation by email to join GitLab's account. Please set up your account by following the instructions in the invitation.
        * Please make an effort to combine expense reports within a short span of time as it will save the company money in excessive Expensify fees.
    *  If you are new to Expensify and would like a brief review, please see [Getting Started](http://help.expensify.com/getting-started/)
    *  For step by step instructions on creating, submitting, and closing a report please see [Create, Submit, Close](http://help.expensify.com/reports-create/)
    *  If you are an employee with a company credit card, your company credit card charges will automatically be fed to a new Expensify report each month. Please attach receipts for these expenses (per the Expense Policy, see below) within 5 business days after the end of the month. These amounts will not be reimbursed to you but Expensify provides a platform for documenting your charges correctly.
   * **Expense Policy**
      * Max Expense Amount - [5,000 USD or 5,000 EUR](/handbook/people-operations/global-compensation/#exchange-rates)
      * Receipt Required Amount - [25 USD or 25 EUR](/handbook/people-operations/global-compensation/#exchange-rates)
      * **Partial Reimbursement** - If the item you wish to purchase exceeds the amount listed in the recommended prices listed above, you may submit an expense for partial reimbursement. For example, if the recommended monitor price is $350 USD, and you prefer a monitor that is $500 USD, you may submit for a partial reimbursement up to $350. You should include your original receipt of $500 while only claiming $350. This policy applies as of 2018-09-14, it can not be applied for purchases made before this date. If you have questions or need an exception, please contact your manager and/or People Operations.
1. Repairs to company issued equipment:
  * Please notify People Ops if your equipment appears to be damaged, defective, or in need of repair. People Ops can advise on next steps to ensure you have the proper equipment to work.
  * Repeated mistakes and mistreatment of company property may result in further disciplinary action.
