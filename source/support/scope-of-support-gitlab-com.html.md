---
layout: markdown_page
title: Support
description: "Visit the GitLab support page to find product support links and to contact the support team."
---

## Scope of Support for GitLab.com (Bronze, Silver, Gold subscriptions)
GitLab.com has a full team of Site Reliability Engineers and Production Engineers monitoring its status 24/7. This means that often,
by the time you notice something is amiss, there's someone already looking into it.

We recommend that all GitLab.com customers follow [`@gitlabstatus`](https://twitter.com/gitlabstatus) on Twitter and use our status page at
https://status.gitlab.com to keep informed of any incidents.

**Note**: If you obtained a Gold subscription as part of [GitLab's Open Source or Education programs](https://about.gitlab.com/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/),
support is **not** included unless purchased separately. Please see the [GitLab OSS License/Subscription Details](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/blob/master/README.md#licensesubscription-details) for additional details


### Outside of the Scope of Support for GitLab.com

| Out of Scope       | Example        | What's in-scope then?   |
|--------------------|----------------|-------------------------|
| 3rd party applications and integrations | *I can't get Jenkins to run builds kicked off by GitLab. Please help me figure out what is going on with my Jenkins server* | GitLab Support can help ensure that Gitlab is providing properly formatted data to 3rd party applications and integrations in the bare-minimum configuration. |
| Troubleshooting non-GitLab components | *How do I merge a branch?* | .com Support will happily answer any questions and help troubleshoot any of the components of GitLab |
| Consulting on language or environment-specific configuration | *I want to set up a YAML linter CI task for my project. How do I do that?* | The Support Team will help you find the GitLab documentation for the related feature and can point out common pitfalls when using it. |

Please also review the items outlined in the [Outside of the Scope of Support for all tiers](index.html#outside-of-the-scope-of-support-for-all-tiers) section.